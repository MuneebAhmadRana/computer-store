import { Bank } from "./objects/bank.js";
import { Work } from "./objects/work.js";
import { getLaptopList } from "./api/api.js";

function App() {
  //instantiation of bank, work and laptop objects
  const bank = new Bank(0, 0, false);
  const work = new Work(0);
  let laptops = [];
  let selectedLaptop = null;

  //DOM elements
  this.elBtnBankLoan = document.getElementById("btn-bank-get-loan");
  this.elBtnWorkTransferBank = document.getElementById("btn-work-transfer-bank");
  this.elBtnWork = document.getElementById("btn-work-to-get-pay");
  this.elDivBtnBank = document.getElementById("bank-btn-div");
  this.elLblWorkPay = document.getElementById("lbl-current-pay-amount");
  this.elLblBankBalance = document.getElementById("lbl-bank-balance-amount");
  this.elDivOutstandingLoan = document.getElementById("bank-outstanding-loan-div");
  this.elLblOutstandingLoan = document.getElementById("lbl-outstanding-loan-amount");
  this.elBtnRepayLoan = document.getElementById("btn-bank-repay-loan");
  this.elLaptopSelect = document.getElementById("laptop-select");
  this.elLaptopName = document.getElementById("laptop-name");
  this.elLaptopDescription = document.getElementById("laptop-description");
  this.elLaptopPrice = document.getElementById("laptop-price");
  this.elLaptopImage = document.getElementById("laptop-image");
  this.elLaptopRating = document.getElementById("laptop-rating");
  this.elBuyLaptopButton = document.getElementById("buy-laptop-button");
  this.elDivLaptopOverview = document.getElementById("overview-section");
  this.elDivNoLaptop = document.getElementById("no-laptop");

  //Init function -- Contains Event Listeners and First time Render
  this.init = function () {
    this.elLaptopSelect.value = -1;

    this.elBtnBankLoan.addEventListener("click", this.handleGetLoan.bind(this));
    this.elBtnWorkTransferBank.addEventListener("click",this.handleTransfer.bind(this));
    this.elBtnWork.addEventListener("click", this.handleAddPay.bind(this));
    this.elBtnRepayLoan.addEventListener("click",this.handleRepayLoan.bind(this));
    this.elLaptopSelect.addEventListener("change",this.onLaptopChange.bind(this));
    this.elBuyLaptopButton.addEventListener("click",this.handleBuyLaptop.bind(this));
    
    this.populateLaptopSelect();
    this.render();
  };

  // this function renders the DOM elements initially and after change
  this.render = function () {
    this.elLblWorkPay.innerText = work.getWorkBalance() + " Kr";
    this.elLblBankBalance.innerText = bank.getBankBalance() + " Kr";
    this.elLblOutstandingLoan.innerText = bank.getOutstandingLoan() + " Kr";
    if (bank.getLoanStatus() != true) {
      this.elDivOutstandingLoan.style.visibility = "hidden";
      this.elBtnRepayLoan.style.visibility = "hidden";
      return;
    }
    this.elDivOutstandingLoan.style.visibility = "visible";
    this.elBtnRepayLoan.style.visibility = "visible";
    if (this.elLaptopSelect.value == -1) {
      this.elBuyLaptopButton.style.visibility = "hidden";
    }
  };

  //This function takes loan
  this.handleGetLoan = function () {
    if (bank.getBankBalance() <= 0) {
      alert("Not sufficient bank balance");
      return;
    } else if (bank.getLoanStatus() == true) {
      alert("You have already taken a loan");
      return;
    }
    const loanAmount = prompt("How much would you like to loan?");
    if (loanAmount > bank.getBankBalance() * 2) {
      alert("Unfortunately you do not have the current balance to take the required amount of loan");
      return;
    }
    if (loanAmount == null) {
      return;
    }
    bank.addToBankBalance(parseInt(loanAmount));
    bank.addOutstandingLoan(parseInt(loanAmount));
    bank.setLoanStatus(true);
    this.render();
  };

  //This function transfers money from work to bank
  this.handleTransfer = function () {
    if (work.getWorkBalance() <= 0) {
      alert("You do not have anything to transfer");
      return;
    }
    if (bank.getLoanStatus() != true) {
      bank.addToBankBalance(work.getWorkBalance());
      work.setWorkBalanceToZero();
    } else {
      const tenPercent = work.getWorkBalance() * 0.1;
      bank.addOutstandingLoan(tenPercent);
      bank.addToBankBalance(work.getWorkBalance() - tenPercent);
      work.setWorkBalanceToZero();
    }
    this.render();
  };

  //Adds 100 to the Work Balance everytime the button is clicked
  this.handleAddPay = function () {
    work.add100ToWorkBalance();
    this.render();
  };

  //this function repays loan
  this.handleRepayLoan = function () {
    if (work.getWorkBalance() < bank.getOutstandingLoan()) {
      alert("You do not have enough money to pay the loan");
      return;
    }
    work.payLoan(bank.getOutstandingLoan());
    bank.setOutstandingLoanToZero();
    bank.setLoanStatus(false);
    this.render();
  };

  //Invokes when a laptop is bought using the buy button
  this.handleBuyLaptop = function () {
    const laptopPrice = selectedLaptop.price;
    if (laptopPrice > bank.getBankBalance()) {
      alert("Insufficient Bank Balance.");
      return;
    }
    bank.setBankBalance(bank.getBankBalance() - laptopPrice);
    this.render();
    alert(`Congratulations. You just bought a ${selectedLaptop.name}`);
  };

  //Invokes to show the selected Laptop
  this.showLaptop = (selectedLaptop) => {
    if (selectedLaptop == null) {
      this.elDivLaptopOverview.style.visibility = "hidden";
      this.elDivNoLaptop.style.display = "block";
      this.elBuyLaptopButton.style.visibility = "hidden";
      return;
    }
    this.elDivLaptopOverview.style.visibility = "visible";
    this.elDivNoLaptop.style.display = "none";
    this.elBuyLaptopButton.style.visibility = "visible";
    this.elLaptopName.innerText = selectedLaptop.name;
    this.elLaptopDescription.innerText = selectedLaptop.description;
    this.elLaptopPrice.innerText = selectedLaptop.price + " Kr";
    this.elLaptopImage.src = selectedLaptop.image;
  };

  //Renders the laptop that is chosen
  this.onLaptopChange = function () {
    if (this.elLaptopSelect.value == -1) {
      this.showLaptop(null);
    }
    selectedLaptop = laptops.find((laptop) => {
      return laptop.id == this.elLaptopSelect.value;
    });
    this.showLaptop(selectedLaptop);
    this.render();
  };

  //Async function that populates Laptop Select element and uses API call
  this.populateLaptopSelect = async function () {
    const pickALaptop = document.createElement("option");
    pickALaptop.innerText = "Pick a Laptop";
    pickALaptop.value = -1;
    this.elLaptopSelect.appendChild(pickALaptop);
    try {
      laptops = await getLaptopList();
      laptops.forEach((laptop) => {
        const option = document.createElement("option");
        option.innerText = laptop.name;
        option.value = laptop.id;
        this.elLaptopSelect.appendChild(option);
      });
    } catch (error) {
      console.log(error);
    }
  };
}

new App().init();
