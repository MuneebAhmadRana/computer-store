const BASE_URL = "http://localhost:3000";

function extractJsonResponse(response) {
  return response.json();
}
export function getLaptopList() {
  return fetch(`${BASE_URL}/computers`).then(extractJsonResponse);
}
