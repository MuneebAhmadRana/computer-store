# The Computer Store
The program executes as intended on Live Server

The Computer Json Server is required and needs to be running to fetch Laptops that are shown on the webpage. The server is provided as another repository on my GitLab



## Description

This application allows the user to buy Laptops based on their current bank balance.

## Tasks to be done:

### 1. Bank

1. Bank Balance: Component that shows the current bank balance
2. Outstanding Loan: Component that becomes visible if the person takes a loan
3. Get a loan (Button): Will attempt to get a loan from the bank by showing a "prompt" popup box that allows the person to enter the amount

#### Constraints

1. The person cannot get a loan more than double of their bank balance
2. The person cannot get more than one bank loan before buying a computer
3. Once the person has a loan, they must pay it back before getting another loan

### 2. Work

1. Work Balance (Pay): Should show how much money the person has earned by working. This money is not part of the bank balance of the person
2. Bank Button: This button must transfer the money from the person's Work balance to their bank balance
3. Work Button: The work button must increase the persons work balance (pay) by 100 on each click. -------> DONE
4. Repay Loan Button: Once the person has a loan, a new button should appear. Upon clicking this button, the full value of the persons work balance should go towards the outstanding loan and not the bank account

#### Constraints

1. If the person has an outstanding loan, 10% of the salary MUST first be deducted and transfered to the outstanding loan amount
2. The balance after the 10% deduction may be transferred to the bank account

### 3. Laptops

#### 3.1 Laptop Selection

1. Select box that shows at least 4 different types of laptops.
2. Each laptop must have a unique name and price.
3. Find a way to store a name, price, description, feature list and image link for each laptop.
4. The feature list of the selected laptop must be displayed in the Laptop Section

#### 3.2 Info Section

1. A large box at the bottom or the "Info Section" is where the image, name and description of the laptop must be displayed.
2. A buy now Button: Final action of the website. This button will attempt to "buy" a laptop and validate whether the bank balance is sufficient to purchase the selected laptop
3. If the bank balance is not enough, a message must be shown that the person can not afford the laptop
4. When the person does have sufficient money to purchase a laptop, the amount must be deducted from the bank balance and the person must receive a message that they are now the new owner of the laptop.
