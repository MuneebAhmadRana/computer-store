export class Work {
  //constructor
  constructor(workBalance = 0) {
    this.workBalance = workBalance;
  }

  //Getters and Setters
  getWorkBalance() {
    return this.workBalance;
  }
  setWorkBalanceToZero() {
    this.workBalance = 0;
  }
  //Adds 100 to work balance whenever the work button is clicked
  add100ToWorkBalance() {
    this.workBalance += 100;
  }
  //Extracts the amount from work, whenever the outstanding loan amount is paid
  payLoan(amount) {
    this.workBalance -= amount;
  }
}
