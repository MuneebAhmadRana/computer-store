export class Bank {
  //constructor
  constructor(bankBalance, outstandingLoan = 0, loanStatus = false) {
    this.bankBalance = bankBalance;
    this.outstandingLoan = outstandingLoan;
    this.loanStatus = loanStatus;
  }

  //Getters and Setters

  getBankBalance() {
    return this.bankBalance;
  }

  setBankBalance(amount) {
    this.bankBalance = amount;
  }

  getOutstandingLoan() {
    return this.outstandingLoan;
  }

  addOutstandingLoan(loan) {
    this.outstandingLoan += loan;
  }

  setOutstandingLoanToZero() {
    this.outstandingLoan = 0;
  }

  addToBankBalance(amount) {
    this.bankBalance += amount;
  }

  getLoanStatus() {
    return this.loanStatus;
  }

  setLoanStatus(bool) {
    this.loanStatus = bool;
  }
}
